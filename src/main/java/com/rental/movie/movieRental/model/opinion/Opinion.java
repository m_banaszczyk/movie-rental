package com.rental.movie.movieRental.model.opinion;

import com.rental.movie.movieRental.model.Movie;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Opinion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userComment;
    private Double rating;

    @ManyToOne
    private Movie movie;

}
