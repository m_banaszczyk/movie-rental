package com.rental.movie.movieRental.repositories;

import com.rental.movie.movieRental.model.Delivery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

    @Repository
    public interface DeliveryRepository extends CrudRepository <Delivery, Long> {


    }


