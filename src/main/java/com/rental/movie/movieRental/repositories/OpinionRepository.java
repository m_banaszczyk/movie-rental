package com.rental.movie.movieRental.repositories;

import com.rental.movie.movieRental.model.opinion.Opinion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OpinionRepository extends CrudRepository<Opinion,Long> {
}
