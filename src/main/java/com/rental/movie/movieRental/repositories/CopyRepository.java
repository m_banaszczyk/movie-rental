package com.rental.movie.movieRental.repositories;

import com.rental.movie.movieRental.model.Copy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CopyRepository extends CrudRepository<Copy, Long> {
}
