package com.rental.movie.movieRental.repositories;

import com.rental.movie.movieRental.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<Order,Long> {
}
