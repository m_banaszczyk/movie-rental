package com.rental.movie.movieRental.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class indexController {
    @GetMapping({"/","","/index"})
    public String index() {

//        List<Genre> genres = new LinkedList<>();
//        Arrays.stream(Genre.values()).sorted().forEach(genres::add);
//        model.addAttribute("genres", genres);
        return "index";

    }

    @GetMapping("/notImplemented")
    public String notImplemented() {
        return "notImplemented";
    }

}
