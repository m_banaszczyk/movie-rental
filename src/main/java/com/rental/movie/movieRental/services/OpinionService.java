package com.rental.movie.movieRental.services;

import com.rental.movie.movieRental.model.opinion.Opinion;

import java.util.List;

public interface OpinionService {


    void save(Opinion opinion);

    Opinion getById(Long id);

    List<Opinion> getAll();

    void update(Opinion opinion);

    void delete(Long id);
}
