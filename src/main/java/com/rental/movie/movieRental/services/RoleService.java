package com.rental.movie.movieRental.services;

import com.rental.movie.movieRental.model.Role;


public interface RoleService {

    Role save(Role role);

    Role findByName(String name);
}
