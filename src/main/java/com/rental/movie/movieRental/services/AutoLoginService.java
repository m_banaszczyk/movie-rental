package com.rental.movie.movieRental.services;

public interface AutoLoginService {
    void autoLogin(String username, String password);
}
