package com.rental.movie.movieRental.services;

import com.rental.movie.movieRental.model.Delivery;

import java.util.List;

public interface DeliveryService {

    void save (Delivery delivery);

    Delivery getById (Long id);

    List<Delivery> getAll();

    void update(Delivery delivery);

    void delete(Long id);





}
