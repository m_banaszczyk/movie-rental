package com.rental.movie.movieRental.services;

import com.rental.movie.movieRental.model.Genre;
import com.rental.movie.movieRental.model.Movie;

import java.util.List;
import java.util.Set;

public interface MovieService {

    void save(Movie movie);

    Movie getById(Long id);

    List<Movie> getAll();

    void update(Movie movie);

    void delete(Long id);

    Movie findByTitle(String Title);

    Movie findByDirector(String director);

    Movie findByGenre(Genre genre);
//    Movie findByGenresIn(List<Genre> genre); propozycja przemka

    Set<Movie> findAllByGenre(Genre genre);


}
