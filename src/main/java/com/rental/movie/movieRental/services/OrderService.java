package com.rental.movie.movieRental.services;

import com.rental.movie.movieRental.model.Order;

import java.util.List;

public interface OrderService{

    void save(Order order);

    Order getById(Long id);

    List<Order> getAll();

    void update(Order order);

    void delete(Long id);

}
