package com.rental.movie.movieRental.services.impl;

import com.rental.movie.movieRental.model.Role;
import com.rental.movie.movieRental.repositories.RoleRepository;
import com.rental.movie.movieRental.services.RoleService;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }
}
